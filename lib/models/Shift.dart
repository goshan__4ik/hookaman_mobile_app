class Shift {
  final int id;
  final String startDate;
  final String endDate;
  final String state;

  Shift({this.id, this.startDate, this.endDate, this.state});

  factory Shift.fromJson(Map<String, dynamic> json) {
    return Shift(
      id: json['id'],
      startDate: json['startDate'],
      endDate: json['endDate'],
      state: json['state'],
    );
  }

  bool isActive() {
    return state == 'active';
  }
}