class Order {
  final int id;
  final String startDate;
  final String state;
  final int table;

  Order({this.id, this.startDate, this.state, this.table});

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      id: json['id'],
      startDate: json['startDate'],
      state: json['state'],
      table: json['table'],
    );
  }

  bool isActive() {
    return state == 'active';
  }
}