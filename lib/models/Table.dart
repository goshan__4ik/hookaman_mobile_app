class Table {
  final int id;
  final int number;

  Table({this.id, this.number});

  factory Table.fromJson(Map<String, dynamic> json) {
    return Table(
      id: json['id'],
      number: json['number']
    );
  }

}