import 'package:R2L/AppConfig.dart';
import 'package:R2L/main.dart';
import 'package:R2L/screens/Token.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:R2L/services/AuthService.dart';
import 'package:R2L/services/ShiftService.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void displayDialog(context, title, text) => showDialog(
    context: context,
    builder: (context) =>
        AlertDialog(
            title: Text(title),
            content: Text(text)
        ),
  );

  Future<String> attemptLogIn(String username, String password) async {
    AuthService authService = new AuthService(AppConfig.getInstance);
    return await authService.getApiToken(username, password);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Log In"),),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextField(
                controller: _usernameController,
                decoration: InputDecoration(
                    labelText: 'Username'
                ),
              ),
              TextField(
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(
                    labelText: 'Password'
                ),
              ),
              FlatButton(
                  child: Text("Log In"),
                  onPressed: () async {
                    var username = _usernameController.text;
                    var password = _passwordController.text;
                    var token = await attemptLogIn(username, password);
                    if(token != null) {
                      new ApiTokenService().storeToken(new Token(token));
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new ShiftsScreen(title: 'Смены', shiftService: new ShiftService(AppConfig.getInstance, ApiTokenService()),)
                          )
                      );
                    } else {
                      displayDialog(context, "An Error Occurred", "No account was found matching that username and password");
                    }
                  }
              )
            ],
          ),
        )
    );
  }
}