import 'dart:async';

import 'package:R2L/AppConfig.dart';
import 'package:R2L/models/Order.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:R2L/services/OrdersService.dart';
import 'package:R2L/widgets/stopwatch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderScreen extends StatefulWidget {
  OrderScreen({Key key, this.title, this.order}) : super(key: key);
  final String title;
  final Order order;
  final OrdersService ordersService = new OrdersService(AppConfig.getInstance, new ApiTokenService());

  @override
  State<StatefulWidget> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {

  bool flag = true;
  Stream<int> timeStream;
  bool initialized = false;
  StreamSubscription<int> timerSubscription;
  int spentHours;

  @override
  void dispose() {
    timerSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Order order = widget.order;
    if (!initialized) {
      initStopWatch(DateTime.parse(order.startDate));
    }
    int cost = _calcCost();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                Navigator.pop(context);
                print('order closed');
              },
              child: Text("Завершить"),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ]
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Text('Проведенное время:'),
            FlutterStopWatch(timerStream: timeStream),
            Text('Сумма:'),
            Text(
              cost.toString(),
              style: TextStyle(
                fontSize: 70.0,
              ),
            ),
            //FlutterStopWatch()
          ],
        ),
      ),
    );
  }

  int _calcCost() {
    if (spentHours == null) {
      return 450;
    }
    return spentHours == 1 ? 450 : 450 + (350 * (spentHours - 1));
  }

  Stream<int> stopWatchStream(int startValue) {
    StreamController<int> streamController;
    Timer timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = startValue;

    void stopTimer() {
      if (timer != null) {
        timer.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);
      if (!flag) {
        stopTimer();
      }
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream.asBroadcastStream(onCancel: (sub) => sub.cancel());
  }

  void initStopWatch(DateTime startedAt) {
    timeStream = stopWatchStream(DateTime.now().difference(startedAt).inSeconds);
    timerSubscription = timeStream.listen((int newTick) {
      var hours = ((newTick / (60 * 60)) % 60)
          .floor();
      if (hours != spentHours) {
        setState(() {
          initialized = true;
          spentHours = hours;
        });
      }
    });
  }
}