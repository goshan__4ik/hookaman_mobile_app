import 'dart:convert';

class Token {

  final String _token;

  Token(this._token);

  String getToken() {
    return _token;
  }

  bool isActive() {
    if (_token == null) {
      return false;
    }
    final Map<String, dynamic> payload = json.decode(
        ascii.decode(
            base64.decode(base64.normalize(_token.split(".")[1]))
        )
    );
    return DateTime.fromMillisecondsSinceEpoch(payload["exp"]*1000).isAfter(DateTime.now());
  }
}