import 'package:R2L/dialogs/CreateOrderDialog.dart';
import 'package:R2L/models/Order.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:R2L/services/OrdersService.dart';
import 'package:R2L/services/ShiftService.dart';
import 'package:R2L/widgets/OrdersListView.dart';
import 'package:flutter/material.dart';

import '../AppConfig.dart';

class ShiftScreen extends StatefulWidget {
  ShiftScreen({Key key, this.title, this.shiftId, this.active}) : super(key: key);
  final String title;
  final int shiftId;
  final bool active;
  final OrdersService ordersService = new OrdersService(AppConfig.getInstance, new ApiTokenService());

  @override
  State<StatefulWidget> createState() => _ShiftScreenState();
}

class _ShiftScreenState extends State<ShiftScreen> {

  @override
  Widget build(BuildContext context) {
    final shiftId = widget.shiftId;
    List<int> availableTables = [1, 2];
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          if (widget.active)
            FlatButton(
            textColor: Colors.white,
            onPressed: () async {
              await new ShiftService(AppConfig.getInstance, new ApiTokenService()).closeShift(shiftId);
              Navigator.pop(context);
            },
            child: Text("Завершить"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: FutureBuilder<List<Order>>(
        future: widget.ordersService.fetchOrders(shiftId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Order> orders = snapshot.data;
            return OrdersListView(orders: orders);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return CircularProgressIndicator();
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return CreateOrderDialog(availableTables,
                        (orderParams) async {
                      await widget.ordersService.createOrder(shiftId, orderParams);
                      setState(() {});
                    });
              }
          );
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}