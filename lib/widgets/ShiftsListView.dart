import 'package:R2L/models/Shift.dart';
import 'package:R2L/screens/ShiftScreen.dart';
import 'package:flutter/material.dart';

import '../TimeUtils.dart';

class ShiftsListView extends StatelessWidget {
  ShiftsListView({Key key, this.shifts}) : super(key: key);
  final List<Shift> shifts;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: shifts.length,
        itemBuilder: (context, index) {
          final shift = shifts[index];
          final date = shiftWidgetDateFormat.format(DateTime.parse(shift.startDate));
          return _shiftWidget(date, shift.id, shift.state, shift.isActive(), context);
        });
  }

  GestureDetector _shiftWidget(String date, int shiftId, String state, bool active, BuildContext context) => GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ShiftScreen(title: 'Смена от $date', shiftId: shiftId, active: active)),
        );
      },
      child: Card(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Text('Смена от $date', style: TextStyle(fontWeight: FontWeight.bold),),
              new Text('Статус: $state'),
              // if (shift.isOpened()) ... [
              //   new Text('Безнал: ${shift.nonCacheSum}'),
              //   new Text('Наличка: ${shift.cacheSum}'),
              //   new Text('Выручка: ${shift.proceeds}')
              // ]
            ],
          ),
        ),
      )
  );
}