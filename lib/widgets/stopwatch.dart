import 'dart:async';

import 'package:flutter/material.dart';

class FlutterStopWatch extends StatefulWidget {

  FlutterStopWatch({this.timerStream});

  Stream<int> timerStream;


  @override
  _FlutterStopWatchState createState() => _FlutterStopWatchState();
}

class _FlutterStopWatchState extends State<FlutterStopWatch> {
  bool initialized = false;
  bool flag = true;
  StreamSubscription<int> timerSubscription;
  String hoursStr = '00';
  String minutesStr = '00';
  String secondsStr = '00';

  void initStopWatch() {
    if (!initialized) {

      timerSubscription = widget.timerStream.listen((int newTick) {

        setState(() {
          initialized = true;
          hoursStr = ((newTick / (60 * 60)) % 60)
              .floor()
              .toString()
              .padLeft(2, '0');
          minutesStr = ((newTick / 60) % 60)
              .floor()
              .toString()
              .padLeft(2, '0');
          secondsStr =
              (newTick % 60).floor().toString().padLeft(2, '0');
        });
      });
    }
  }

  @override
  void dispose() {
    timerSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    initStopWatch();
    return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "$hoursStr:$minutesStr:$secondsStr",
              style: TextStyle(
                fontSize: 70.0,
              ),
            ),
          ],
    );
  }
}