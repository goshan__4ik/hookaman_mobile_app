import 'package:R2L/TimeUtils.dart';
import 'package:R2L/models/Order.dart';
import 'package:R2L/screens/OrderScreen.dart';
import 'package:flutter/material.dart';


class OrdersListView extends StatelessWidget {
  OrdersListView({Key key, this.orders}) : super(key: key);
  final List<Order> orders;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: orders.length,
        itemBuilder: (context, index) {
          return _OrderWidget(context, orders[index]);
        });
  }

  GestureDetector _OrderWidget(BuildContext context, Order order) {
    final date = timeFormat.format(DateTime.parse(order.startDate));
    String state;
    return GestureDetector(
      child: Card(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Text('Заказ от $date',
                style: TextStyle(fontWeight: FontWeight.bold),),
              new Text(
                'Стол ${order.table}', style: TextStyle(fontWeight: FontWeight.bold),),
              new Text('Статус: ${order.state}'),
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => OrderScreen(title: 'Стол ${order.table}', order: order)),
        );
      },
    );
  }
}