import 'package:intl/intl.dart';

DateFormat serverFormat = new DateFormat("yyyy-MM-ddTHH:mm:ss.SSSz");

DateFormat shiftWidgetDateFormat = new DateFormat("dd.MM");

DateFormat timeFormat = new DateFormat("HH:mm");