import 'package:R2L/AppConfig.dart';
import 'package:R2L/dialogs/CreateOrderDialog.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:R2L/models/Order.dart';

class OrdersService {

  AppConfig _appConfig;
  ApiTokenService _apiTokenService;

  OrdersService(this._appConfig, this._apiTokenService);

  Future<List<Order>> fetchOrders(int shiftId) async {
    final Map<String, String> headers = await _getRequestHeaders();
    final response = await http.get(_getShiftsApiUrl(shiftId), headers: headers);

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((order) => new Order.fromJson(order)).toList();
    } else {
      throw Exception('Failed to load Orders from API');
    }
  }

  Future<Order> createOrder(int shiftId, CreateOrderParams order) async {
    final Map<String, String> headers = await _getRequestHeaders();
    final response = await http.post(_getShiftsApiUrl(shiftId),
        headers: headers,
        body: jsonEncode(order.toJson()));

    if (response.statusCode == 201) {
      //TODO
      return Order.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load Orders from API');
    }
  }

  String _getShiftsApiUrl(int shiftId) {
    return _appConfig.getBaseUrlApi() + "/shifts/" + shiftId.toString() + "/orders";
  }

  Future<Map<String, String>> _getRequestHeaders() async {
    final token = await _apiTokenService.getToken();
    return {
      'Accept': 'application/json',
      'Authorization': token.getToken()
    };
  }

}