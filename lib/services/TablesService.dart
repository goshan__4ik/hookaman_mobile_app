import 'package:R2L/AppConfig.dart';
import 'package:R2L/models/Table.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class TableService {

  AppConfig appConfig;
  ApiTokenService apiTokenService;

  TableService(AppConfig appConfig, ApiTokenService apiTokenService) {
    this.appConfig = appConfig;
    this.apiTokenService = apiTokenService;
  }

  Future<List<Table>> getAvailableTables() async {
    final Map<String, String> headers = await _getRequestHeaders();
    final response = await http.get(_getTableApiUrl(), headers: headers);

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((table) => new Table.fromJson(table)).toList();
    } else {
      throw Exception('Failed to load shifts from API');
    }
  }

  String _getTableApiUrl() {
    return appConfig.getBaseUrlApi() + "/tables";
  }

  Future<Map<String, String>> _getRequestHeaders() async {
    final token = await apiTokenService.getToken();
    return {
      'Accept': 'application/json',
      'Authorization': token.getToken()
    };
  }
}