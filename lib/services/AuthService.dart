import 'dart:convert';
import 'package:R2L/AppConfig.dart';
import 'package:http/http.dart' as http;

class AuthService {

  AppConfig appConfig;

  AuthService(AppConfig appConfig) {
    this.appConfig = appConfig;
  }

  Future<String> getApiToken(String login, String password) async {
    final url = appConfig.getBaseUrlApi() + "/authenticate";
    final response = await http.post(url,
        headers: {'Accept': 'application/json'},
        body: {"login": login,"password": password}
        );

    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = json.decode(response.body);
      return jsonResponse['auth_token'];
    } else {
      throw Exception('Failed to load shifts from API');
    }
  }
}