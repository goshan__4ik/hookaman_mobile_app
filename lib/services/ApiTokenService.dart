import 'package:R2L/screens/Token.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ApiTokenService {

   static const String KEY = "R2L_API_KEY";

  FlutterSecureStorage storage;

  ApiTokenService() {
    this.storage = new FlutterSecureStorage();
  }

  Future<Token> getToken() async {
    String apiKey = await storage.read(key: KEY);
    return new Token(apiKey);
  }

  Future<bool> deleteToken() async {
    await storage.delete(key: KEY);
    return true;
  }

  Future<bool> storeToken(Token token) async {
    if (token.isActive()) {
      await storage.write(key: KEY, value: token.getToken());
      return true;
    }
    return false;
  }

}