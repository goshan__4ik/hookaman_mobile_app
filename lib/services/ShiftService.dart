import 'package:R2L/AppConfig.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../models/Shift.dart';

class ShiftService {

  AppConfig appConfig;
  ApiTokenService apiTokenService;

  ShiftService(AppConfig appConfig, ApiTokenService apiTokenService) {
    this.appConfig = appConfig;
    this.apiTokenService = apiTokenService;
  }

  Future<List<Shift>> fetchShifts() async {
    final Map<String, String> headers = await _getRequestHeaders();
    final response = await http.get(_getShiftsApiUrl(), headers: headers);

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((shift) => new Shift.fromJson(shift)).toList();
    } else {
      throw Exception('Failed to load shifts from API');
    }
  }

  Future<int> createShift() async {
    final Map<String, String> headers = await _getRequestHeaders();
    final response = await http.post(_getShiftsApiUrl(), headers: headers);

    if (response.statusCode == 200) {
      Map<String, dynamic> shift = json.decode(response.body);
      return shift['id'];
    } else {
      throw Exception('Failed to load shifts from API');
    }
  }

  Future<bool> closeShift(int shiftId) async {
    final Map<String, String> headers = await _getRequestHeaders();
    final String url = _getShiftsApiUrl() + shiftId.toString() + '?action=close';
    final response = await http.post(url, headers: headers);

    if (response.statusCode == 200) {
     return true;
    } else {
      throw Exception('Failed to load shifts from API');
    }
  }

  String _getShiftsApiUrl() {
    return appConfig.getBaseUrlApi() + "/shifts/";
  }

  Future<Map<String, String>> _getRequestHeaders() async {
    final token = await apiTokenService.getToken();
    return {
      'Accept': 'application/json',
      'Authorization': token.getToken()
    };
  }
}