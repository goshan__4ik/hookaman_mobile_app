import 'package:R2L/AppConfig.dart';
import 'package:R2L/screens/LoginScreen.dart';
import 'package:R2L/screens/ShiftScreen.dart';
import 'package:R2L/screens/Token.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:R2L/services/ShiftService.dart';
import 'package:flutter/material.dart';
import 'TimeUtils.dart';
import 'colors.dart';
import 'models/Shift.dart';
import 'widgets/ShiftsListView.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    ApiTokenService apiTokenService = new ApiTokenService();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: primaryBlack,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: FutureBuilder<Token>(
        future: apiTokenService.getToken(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Token token = snapshot.data;
            if (token.isActive()) {
              return ShiftsScreen(title: 'Смены', shiftService: new ShiftService(AppConfig.getInstance, apiTokenService));
            }
            return LoginScreen();
          } else if (snapshot.hasError) {
            return LoginScreen();
          }
          return Scaffold(
            appBar: AppBar(
              title: Text('R2l'),
            ),
            body: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class ShiftsScreen extends StatelessWidget {
  ShiftsScreen({Key key, this.title, this.shiftService}) : super(key: key);
  final String title;
  final ShiftService shiftService;

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<List<Shift>>(
      future: shiftService.fetchShifts(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Shift> shifts = snapshot.data;
          return _scaffold(shifts, context);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  Scaffold _scaffold(List<Shift> shifts, context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ShiftsListView(shifts: shifts),
      floatingActionButton: shifts.where((shift) => shift.isActive()).isEmpty ? _createShiftButton(context) : Container(), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _createShiftButton(context) {
    return FloatingActionButton(
      onPressed: () {
        shiftService.createShift().then((int shiftId) => {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ShiftScreen(title: 'Смена от ${shiftWidgetDateFormat.format(DateTime.now())}', shiftId: shiftId, active: true)))
        });
      },
      tooltip: 'Increment',
      child: Icon(Icons.add),
    );
  }
}
