import 'TimeUtils.dart';

class OrderSummary {

  final DateTime startTime;
  final DateTime endTime;
  final String description;
  final int productCost;
  final int timeCost;
  final PaymentInfo paymentInfo;

  OrderSummary({this.startTime, this.endTime, this.description, this.productCost,
                this.timeCost, this.paymentInfo});

  factory OrderSummary.fromJson(Map<String, dynamic> json) {
    return OrderSummary(
      startTime: serverFormat.parse(json['startTime']),
      endTime: serverFormat.parse(json['endTime']),
      description: json['description'],
      productCost: json['productCost'],
      timeCost: json['timeCost'],
      paymentInfo: PaymentInfo.fromJson(json['paymentInfo'])
    );
  }

  // TODO fix
  int getSpentTime() {
    Duration spentTime = endTime.difference(startTime);
    int hours = spentTime.inHours;
    if (spentTime.inMinutes % 60 > 0) {
      hours++;
    }
    return hours;
  }
}

class PaymentInfo {

  final bool isPaid;
  final int paidSum;
  final int fullCost;
  final String payMethod;

  PaymentInfo({this.isPaid, this.paidSum, this.fullCost, this.payMethod});

  factory PaymentInfo.fromJson(Map<String, dynamic> json) {
    return PaymentInfo(
      isPaid: json['isPaid'],
      paidSum: json['paidSum'],
      fullCost: json['fullCost'],
      payMethod: json['payMethod']
    );
  }
}

