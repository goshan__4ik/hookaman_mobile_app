import 'package:R2L/AppConfig.dart';
import 'package:R2L/services/ApiTokenService.dart';
import 'package:R2L/services/TablesService.dart';
import 'package:flutter/cupertino.dart' hide Table;
import 'package:flutter/material.dart' hide Table;
import 'package:flutter/services.dart';
import 'package:R2L/models/Table.dart';

class CreateOrderDialog extends StatefulWidget {

  final List<int> availableTables;
  final ValueChanged<CreateOrderParams> onDialogClosed;
  final TableService tableService = new TableService(AppConfig.getInstance, new ApiTokenService());

  CreateOrderDialog(this.availableTables, this.onDialogClosed);

  @override
  State<CreateOrderDialog> createState() => _CreateOrderDialogState();

}

class _CreateOrderDialogState extends State<CreateOrderDialog> {

  int _selectedTable = 0;
  int _personsCount = 0;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Table>>(
        future: widget.tableService.getAvailableTables(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return _createDialog(snapshot.data);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return CircularProgressIndicator();
        },
    );
  }

  AlertDialog _createDialog(List<Table> tables) {
    final tableSelection = new DropdownButton<String>(
      items: tables.map((Table table) {
        return new DropdownMenuItem<String>(
          value: table.number.toString(),
          child: new Text(table.number.toString()),
        );
      }).toList(),
      onChanged: (String table) {
        setState(() {
          _selectedTable = int.parse(table);
        });
      },
      value: _selectedTable == 0 ? widget.availableTables[0].toString() : _selectedTable.toString(),
    );
    final personsCountInput = TextField(
      keyboardType: TextInputType.number,
      onChanged: (String persons) {
        setState(() {
          _personsCount = int.parse(persons);
        });
      },
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
    );
    return AlertDialog(
      title: Text('Создание заказа'),
      contentPadding: EdgeInsets.zero,
      content: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(children: <Widget>[
                Text('Номер стола: '),
                tableSelection
              ],
              ),
              Row(children: <Widget>[
                Text('Количество человек: '),
                Container(
                  width: 15,
                  child: personsCountInput,
                )
              ],
              )
            ],
          )
      ),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Отменить'),
        ),
        FlatButton(
          onPressed: () {
            widget.onDialogClosed(new CreateOrderParams(_selectedTable, _personsCount));
            Navigator.pop(context);
          },
          child: Text('Создать'),
        ),
      ],

    );
  }
}

class CreateOrderParams {
  final int _personsCount;
  final int _table;

  CreateOrderParams(this._table, this._personsCount);

  Map<String, dynamic> toJson() =>
      {
        'table': _table,
        'personsCount': _personsCount,
      };
}