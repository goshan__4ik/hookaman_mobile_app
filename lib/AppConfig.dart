enum Environment { dev, prod }

Map<Environment, String> urls = {
  //Environment.dev: "http://172.20.10.2:3000",
  Environment.dev: "http://192.168.100.8:3000",
  //Environment.dev: "http://192.168.0.46:3000",
  Environment.prod: "http://212.237.14.12:80"
};

class AppConfig {

  static AppConfig _instance;

  AppConfig._();

  static AppConfig get getInstance => _instance ??= AppConfig._();

  Environment getCurrentMode() {
    return Environment.dev;
  }

  String getBaseUrlApi() {
    return urls[getCurrentMode()];
  }

}